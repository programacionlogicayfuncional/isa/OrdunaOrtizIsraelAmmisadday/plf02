(ns plf02.core)

(defn función-associative?-1
 [a]
  (associative? a))

(defn función-associative?-2
 [a]
  (associative? a))

(defn función-associative?-3
 [a]
  (associative? a))

(función-associative?-1 [1 2 3])
(función-associative?-2 '(1 2 3))
(función-associative?-3 "hola")

(defn función-boolean?-1
 [a]
  (boolean? a))

(defn función-boolean?-2
 [a]
  (boolean? a))

(defn función-boolean?-3
 [a]
  (boolean? a))

(función-boolean?-1 false)
(función-boolean?-2 (new Boolean "true"))
(función-boolean?-3 nil)

(defn función-char?-1
 [a]
  (char? a))

(defn función-char?-2
 [a]
  (char? a))

(defn función-char?-3
 [a]
  (char? a))

(función-char?-1 \a)
(función-char?-2 22)
(función-char?-3 (first "hola"))

(defn función-coll?-1
  [a]
 (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 [1 2 3 4 5 6])
(función-coll?-2 (* 1 2))
(función-coll?-3 '("plf" "hola" "hello bro!"))


(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 (* 15M  43))
(función-decimal?-2 10)
(función-decimal?-3 99999999999)

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))


(función-double?-1 100.0)
(función-double?-2 (* 25 0))
(función-double?-3 (new BigDecimal "1"))

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 100)
(función-float?-2 (+ 10.5 2))
(función-float?-3 (* 1 2))

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 'fgxc)
(función-ident?-1 :plf)
(función-ident?-3 (empty [1 2 3]))

(defn función-indexed?-1
[a]  
 (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [:a 1 :b 2 :c 3])
(función-indexed?-2 [1 2 3 4])
(función-indexed?-3 (vector {:a 1 :b 2}))

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
[a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 10.5)
(función-int?-2 (+ 12 5))
(función-int?-3 (/ 10 3))

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3 
  [a]
  (integer? a))

(función-integer?-1 150) 
(función-integer?-2 (int (+ 3.5 6 8)))
(función-integer?-3 (first [\1 \b]))

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 :a)
(función-keyword?-2 "plf")
(función-keyword?-3 (first {:a 5 :b 15}))

(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 50)
(función-list?-2 (range 10))
(función-list?-3 (last [200 15]))

(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a ]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 (first {:a 1 :b 2}))
(función-map-entry?-2 [1 2 5])
(función-map-entry?-3 (last [1 2 2]))

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 {:a 1 :b 2})
(función-map?-2 (inc 10))
(función-map?-3 '("hola"))

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 -100)
(función-nat-int?-2 (+ 10 1))
(función-nat-int?-3 (/ 20 7))

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 100)
(función-number?-2 (last [1 2 3]))
(función-number?-3 (+ 12.2))

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 9999999)
(función-pos-int?-2 (* 100 20))
(función-pos-int?-3 10M)

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

(función-ratio?-1 10)
(función-ratio?-2 (* 1/2 1/8))
(función-ratio?-3 (+ 4 3/4))

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
[a]
  (rational? a))

(función-rational?-1 10000)
(función-rational?-2 (+ 4.5 3))
(función-rational?-3 (+ 2 1/4 3/8))

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 [1 2 3])
(función-seq?-2 (range 100))
(función-seq?-3 '([:a :b]))

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 [])
(función-seqable?-2 {})
(función-seqable?-3 (last [:a]))

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

(función-sequential?-1 [1 2 3])
(función-sequential?-2 (range 1))
(función-sequential?-3 {:a 1 :b 2 :c 3})

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 [1 2 3])
(función-set?-2 {:a 1 :b 2 :c 3})
(función-set?-3 (hash-set 1 2 3))

(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 false)
(función-some?-2 (+ 2 0))
(función-some?-3 [])

(defn función-string?-1
 [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "plf")
(función-string?-2 (first ["hola" 1 4 :c]))
(función-string?-3 (last '("plf" 1)))

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 15)
(función-symbol?-2 (first [:a 1 2 "plf"]))
(función-symbol?-3 (last [1 2 3])) 

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
 (vector? a))

(función-vector?-1 [1 2 34])
(función-vector?-2 (vector '(1 2 3)))
(función-vector?-3 (first [1 4 5]))

(defn función-drop-1
  [a b]
  (drop a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

(función-drop-1 -1 [1 2 3 4])
(función-drop-2 5 [1 2 3 4])
(función-drop-3 2 {:a 1 :b 2 :c 3 :d 4})

(defn función-drop-last-1
  [a b]
  (drop-last a b))

(defn función-drop-last-2
  [a b ]
  (drop-last a b))

(defn función-drop-last-3
  [a b ]
  (drop-last a b))

(función-drop-last-1 5 (list 1 2 3 4))
(función-drop-last-2 2 (vector 1 2 3 4))
(función-drop-last-3 1 {:a 1 :b 2 :c 3 :d 4})

(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
  (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b))

(función-drop-while-1 neg? [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])
(función-drop-while-2 #(> 3 %) [1 2 3 4 5 6])
(función-drop-while-3 #(>= 3 %) [1 2 3 4 5 6])

(defn función-every?-1
  [a b]
  (every? a b))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

(función-every?-1 even? '(2 4 6))
(función-every?-2 string? '("PLF"))
(función-every?-3 number? '(142))

(defn función-filterv-1
  [a b]
  (filterv a b))

(defn función-filterv-2
  [a b]
  (filterv a b))

(defn función-filterv-3
  [a b]
  (filterv a b))

(función-filterv-1 even? (range 10))
(función-filterv-2 even? (range 20))
(función-filterv-2 even? (range 15))

(defn función-group-by-1
  [a b]
  (group-by a b))

(defn función-group-by-2
  [a b]
  (group-by a b))

(defn función-group-by-3
  [a b]
  (group-by a b))

(función-group-by-1 count ["hola" "diego" "plf" "aa" "asdf"])
(función-group-by-2 odd? (range 20))
(función-group-by-3 set ["meat" "mat" "team" "mate" "eat" "tea"])

(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (take a b))

(defn función-iterate-3
  [a b]
  (take a b))

(función-iterate-1 inc 10)
(función-iterate-2 10 (iterate inc 10))
(función-iterate-3 8 (iterate (partial + 5) 10))


(defn función-keep-1
  [a b ]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))
 
(función-keep-1 even? (range 1 10)) 
(función-keep-2 seq [() [] '(1 2 3) [:a :b] nil])
(función-keep-3 #{0 1 2 3} #{2 3 4 5})

(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b ]
  (keep-indexed a b))

(función-keep-indexed-1 vector "plf")
(función-keep-indexed-2 vector [-10 -5 20 100 1457 -20 -11])
(función-keep-indexed-3 list [2 5 3 4 6 7 9 8])

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b ))

(función-map-indexed-1  hash-map "juan")
(función-map-indexed-2  list '(:a :b :c))
(función-map-indexed-3  vector "plf")

(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

(defn función-mapcat-3
  [a b]
  (mapcat a b))

(función-mapcat-1  reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2  reverse [[2 2] [4 8] [2 3]])
(función-mapcat-3  list '([:a :b :c] [1 2 3]))

(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b c]
  (mapv a b c))

(defn función-mapv-3
  [a b ]
  (mapv a b))

(función-mapv-1 inc [1 2 3 4 5])
(función-mapv-2 + [1 2 3] [4 5 6])
(función-mapv-3 #(str "hola " % "!") ["pedro" "jose" "juan"])

(defn función-merge-with-1
  [a b]
  (merge-with a b))

(defn función-merge-with-2
  [a b c]
  (merge-with a b c))

(defn función-merge-with-3
  [a b c]
  (merge-with a b c))

(función-merge-with-1 + ({:a 1} {:b 2} {:b 3}))
(función-merge-with-2 merge {:x {:y {:a 1}}} {:x {:y {:b 2}}})
(función-merge-with-3 into {:a #{1 2 3}, :b #{4 5 6}}{:a #{2 3 7 8}, :c #{1 2 3}})

(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 odd? '(2 4 6))         
(función-not-any?-2 nil? [true false false])
(función-not-any?-3 nil? [true false nil])

(defn función-not-every?-1 
 [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

(función-not-every?-1 odd? '(1 3))
(función-not-every?-2 odd? [1 2 3])
(función-not-every?-3 string? '("plf"))

(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1 even? [1 22 22 5 5])
(función-partition-by-2 #(= 5 %) [10 2 5 4 10])
(función-partition-by-3 identity "Megamente")

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 100 :b 4 :c 80})
(función-reduce-kv-2 #(assoc %1 %1 %3) {} {:d 51 :e 04 :f 13})
(función-reduce-kv-3 #(assoc %1 %1 %3) {} {:c 51 :h 04 :o 13})

(defn función-remove-1
  [a]
  (remove pos? a))

(defn función-remove-2
  [a]
  (remove nil? a))

(defn función-remove-3
  [a]
  (remove even? a))

(función-remove-1 [1 -2 2 -1 3 7 0])
(función-remove-2 [1 nil 2 nil 3 nil])
(función-remove-3 (range 10))

(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 "hola")
(función-reverse-2 ["h" "o" "l" "a"])
(función-reverse-3 {'h' 'i'})

(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 even? '(1 2 3 4))
(función-some-2 true? [false false false])
(función-some-3 #(= 5 %) [1 2 3 4 5])

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 even? '(1 2 3 4))
(función-sort-by-2 count '([ "hola" "aa" "bb"])) 
(función-sort-by-3 first [[1 2] [2 2] [2 3]]) 

(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 (partial >= 3) [10 100 1 20])
(función-split-with-2 odd? [1 3 5 6 7 9])
(función-split-with-3 (partial > 10) [1 3 5 6 7 9])

(defn función-take-1
  [a]
  (take 1 a))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 [1 2 3])
(función-take-2 3 '(1 2 3 4))
(función-take-3 2 {"h" "i"})

(defn función-take-last-1
  [a b]
 (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 1 [1 2 3])
(función-take-last-2 0 '(1 2))
(función-take-last-3 2 {1 2 3 4})


(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 1 (range 10))
(función-take-nth-2 0 (range 5)) 
(función-take-nth-3 2 (range 100))

(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

(función-take-while-1 neg? [-2 -1 0 1 2 4 -10])
(función-take-while-2 neg? [])
(función-take-while-3 integer? [1 2 3 -50 1.0 5.5])

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 [1 2 3] 0 inc) 
(función-update-2 [] 0 #(str "hola" %))
(función-update-3 [4 5 6] 1 inc)


(defn función-update-in-1
  [a b c]
  (update-in a b c))

(defn función-update-in-2
  [a b c]
  (update-in a b c))

(defn función-update-in-3
  [a b c]
  (update-in a b c))

(función-update-in-1 {:a 3} [:a] inc)
(función-update-in-2 {} [] (constantly {:k :v}))
(función-update-in-3 {:a {:b 3}} [:a :b] inc) 





